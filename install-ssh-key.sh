#!/usr/bin/expect -f
lassign $argv IPSrv Login Passwd IdentityFile

set timeout 10

spawn ssh-copy-id -i "$IdentityFile" -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -o ConnectTimeout=1s $Login@$IPSrv

expect {
    -re ".*assword:.*" {
        send "$Passwd\r"
        exp_continue
    }

    -re ".*ERROR:.*" {
        exit 4
    }

    eof {}
}
